<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {


	public function __construct()
    {
            $this->load->database();
    }

	function AddUser($data){
		$this->db->insert('users', $data);

		return $this->db->affected_rows() > 0 ? $this->db->insert_id() : false;
	}

 public function set_session()
    {
        $username = $this->input->post('username');
        $pword = $this->input->post('password');

        $query = $this->db->get_where('contacts', array('Username_c' => $username, 'ArchPassword_c' => $password));
        return $query->row_array();
    }
	function AllDataDetails($table, $field, $value){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field, $value);

		return $this->db->get();
	}

	function Setup($data){
		$this->db->insert('users', $data);

		return $this->db->affected_rows() > 0 ? true : false;
	}

	function SetupDone(){
      $this->db->select('ID');
      $this->db->from('users');
      $this->db->where('user_type','Admin');

      return $this->db->get()->num_rows() > 0 ? true : false;
   }

	function UserDetails($value, $field = 'ID'){
		$this->db->select('ID');
		$this->db->select('email');
		$this->db->select('username');
		$this->db->select('password');
		$this->db->select('user_type');
		$this->db->select('password_reset_key');
		$this->db->select('password_reset_date');
		$this->db->select('status');
		$this->db->from('users');
		$this->db->where($field, $value);

		$result = $this->db->get();

		return $result->num_rows() == 1 ? $result->row() : false;
	}


	function update($value, $field = 'ID'){
      $this->db->select('ID');
      $this->db->select('FisrtName');
      $this->db->select('LastName');
      $this->db->select('Username_c');
     
      $this->db->where($field, $value);

      return $this->db->get()->row();
   }
}
