<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define("SOAP_CLIENT_BASEDIR", APPPATH."third_party/salesforce");
require_once (SOAP_CLIENT_BASEDIR.'/SforceEnterpriseClient.php');

class Salesforce{

	private $USERNAME = "apiuser@archintel.com";
	private $PASSWORD = "archintel2015";
	private $conn = NULL;

	function __construct(){
		$this->conn = new SforceEnterpriseClient();
		$this->conn->createConnection(SOAP_CLIENT_BASEDIR.'/archintel.xml');
		$this->conn->login($this->USERNAME, $this->PASSWORD);
		$this->CI =& get_instance();
	}

	function DownloadMedia($start_date = null){
      $query = 'SELECT Id, Name, Date__c, Account__c, News_Category__c, Media_Title__c, Contenttags__c, Media_Link__c, Media_Summary__c, Media_Image__c, Top_Headline__c, Publish__c FROM Archintel__c WHERE Publish__c = TRUE AND Date__c > '.date('Y-m-d', strtotime($this->CI->Archintel_model->LastMediaDate()));

		$response = $this->conn->query($query);

		$media = array();

		foreach($response->records as $record){
			array_push($media, array(
				'SalesforceID'		=>		$record->Id,
				'Name'				=>		$record->Name,
				'Date__c'			=>		isset($record->Date__c) ? $record->Date__c : null,
				'AccountID'			=>		isset($record->Account__c) ? $record->Account__c : null,
				'News_Category__c'	=>		isset($record->News_Category__c) ? $record->News_Category__c : '',
				'Media_Title__c'	=>		isset($record->Media_Title__c) ? $record->Media_Title__c : '',
				'Contenttags__c'	=>		isset($record->Contenttags__c) ? $record->Contenttags__c : '',
				'Media_Link__c'		=>		isset($record->Media_Link__c) ? $record->Media_Link__c : '',
				'Media_Summary__c'	=>		isset($record->Media_Summary__c) ? $record->Media_Summary__c : '',
				'Media_Image__c'	=>		isset($record->Media_Image__c) ? $record->Media_Image__c : null,
				'Top_Headline__c'	=>		isset($record->Top_Headline__c) ? $record->Top_Headline__c : false,
				'Publish__c'		=>		isset($record->Publish__c) ? $record->Publish__c : false,
			));
		}

		return $media;
	}

	function DownloadAccountDetails($ID){
		$query = 'SELECT Active__c, Name, Id, BillingCity, BillingStreet, BillingCountry, BillingState, BillingPostalCode, Expiration_Date__c, Fax, Industry, Phone, Subscription_Type__c FROM Account WHERE ArchIntelId__c = "'.$ID.'"';

		try{
			$response = $this->conn->query($query);

			$details = array();

			foreach($response->records as $record){
				array_push($details, array(
					'Active__c'		=>	isset($record->Active__c) ? $record->Active__c : false,
					'Name'			=>	isset($record->Name) ? $record->Name : null,
					'SalesforceID'	=>	isset($record->Id) ? $record->Id : null,
					'BillingCity'	=>	isset($record->BillingCity) ? $record->BillingCity : null,
					'BillingStreet'=>	isset($record->BillingStreet) ? $record->BillingStreet : null,
					'BillingCountry'=>	isset($record->BillingCountry) ? $record->BillingCountry : null,
					'BillingState'	=>	isset($record->BillingState) ? $record->BillingState : null,
					'BillingPostalCode'	=>	isset($record->BillingPostalCode) ? $record->BillingPostalCode : null,
					'Expiration_Date__c'	=>	isset($record->Expiration_Date__c) ? $record->Expiration_Date__c : null,
					'Fax'				=>	isset($record->Fax) ? $record->Fax : null,
					'Industry'		=>	isset($record->Industry) ? $record->Industry : null,
					'Phone'			=>	isset($record->Phone) ? $record->Phone : null,
					'Subscription_Type__c'	=>	isset($record->Subscription_Type__c) ? $record->Subscription_Type__c : null,
				));
			}

			return $details;
		}catch(Exception $e){
			return $e->faultstring;
		}
	}

	function DownloadSalesforceAccount($id){
		$query = 'SELECT Active__c, Name, Id, BillingCity, BillingStreet, BillingCountry, BillingState, BillingPostalCode, Expiration_Date__c, Fax, Industry, Phone,Subscription_Type__c FROM Account WHERE Id = \''.$id.'\'';

		try{
			$response = $this->conn->query($query);

			$details = array();

			foreach($response->records as $record){
				array_push($details, array(
					'Active__c'		=>	isset($record->Active__c) ? $record->Active__c : false,
					'Name'			=>	isset($record->Name) ? $record->Name : null,
					'SalesforceID'	=>	isset($record->Id) ? $record->Id : null,
					'BillingCity'	=>	isset($record->BillingCity) ? $record->BillingCity : null,
					'BillingStreet'=>	isset($record->BillingStreet) ? $record->BillingStreet : null,
					'BillingCountry'=>	isset($record->BillingCountry) ? $record->BillingCountry : null,
					'BillingState'	=>	isset($record->BillingState) ? $record->BillingState : null,
					'BillingPostalCode'	=>	isset($record->BillingPostalCode) ? $record->BillingPostalCode : null,
					'Expiration_Date__c'	=>	isset($record->Expiration_Date__c) ? $record->Expiration_Date__c : null,
					'Fax'				=>	isset($record->Fax) ? $record->Fax : null,
					'Industry'		=>	isset($record->Industry) ? $record->Industry : null,
					'Phone'			=>	isset($record->Phone) ? $record->Phone : null,
					'Subscription_Type__c'	=>	isset($record->Subscription_Type__c) ? $record->Subscription_Type__c : null,
				));
			}

			return $details;
		}catch(Exception $e){
			return $e->faultstring;
		}
	}

	function GetContactsByAccountID($id){
		$query = 'SELECT Id, FirstName, LastName, Username__c, ArchPassword__c, AccountId FROM Contact WHERE Account.Id = \''.$id.'\'';

		try{
			$response = $this->conn->query($query);

			$contacts = array();

			foreach($response->records as $record){
				array_push($contacts, array(
					'SalesforceID'			=>	isset($record->Id) ? $record->Id : null,
					'FirstName'				=>	isset($record->FirstName) ? $record->FirstName : null,
					'LastName'				=>	isset($record->LastName) ? $record->LastName : null,
					'Username__c'			=>	isset($record->Username__c) ? $record->Username__c : null,
					'ArchPassword__c'		=>	isset($record->ArchPassword__c) ? $record->ArchPassword__c : null,
					'AccountID'				=>	$id
					));
			}

			return $contacts;
		}catch(Exception $e){
			return $e->faultstring;
		}
	}
}
