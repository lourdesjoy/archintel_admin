
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class manages extends CI_Controller {

   function __construct(){
      parent::__construct();

      $this->load->model('Auth_model');
      $this->load->model('manage');
      $this->load->library('bcrypt');
   }

  public function updateinfo(){
    
           $this->load->helper('form');
     $this->load->library('form_validation');


     $data['infos']=$this->Auth_model->AllDataDetails('users','email',$this->session->userdata('account')['username']);

         $data['content'] = 'auth/updateinfo';
         $this->load->view('tpl/full-page/main', $data);

 }

   public function updatepassword(){
    
           $this->load->helper('form');
     $this->load->library('form_validation');


     $data['infos']=$this->Auth_model->AllDataDetails('users','email',$this->session->userdata('account')['username']);
     
         $data['content'] = 'auth/updatepass';
         $this->load->view('tpl/full-page/main', $data);

 }

 public function edit(){
     
        $this->load->library('bcrypt');

         $id= $this->input->post('id');
         $data = array(
         'username' => $this->input->post('username'),
         'email' => $this->input->post('email'),
       
         
        /* 'password' => $this->bcrypt->hash($this->input->post('password')),*/
         'user_type' => $this->input->post('usertype'),
         );
         $this->manage->update($id,$data);
       



   }

 public function pass(){
     
        $this->load->library('bcrypt');

         $id= $this->input->post('id');
         $data = array(
          
        'password' => $this->bcrypt->hash($this->input->post('password')),
         'user_type' => $this->input->post('usertype'),
         );
         $this->manage->update($id,$data);
       


}
   
 }

 