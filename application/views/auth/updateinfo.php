 
 <div class="row">
         <div class="col-md-8 col-md-offset-2" style="margin-top: 20px;" >
               
               <div class="col-md-12 form-group">
                  <h3>Update Information</h3>
               </div>

               <form method="post" action="<?php echo base_url() . "index.php/manages/edit" ?>" >
               <?php foreach($infos->result() as $info): ?>
                 <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="firstname">ID</label>
                      <input value="<?= $info->ID; ?>"  class="form-control" id="id" name='id' placeholder="First Name"  required>
                  </div>
               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="firstname">UserName</label>
                      <input value="<?= $info->username; ?>" type="text" class="form-control" id="username" name='username' placeholder="First Name"  required>
                  </div>
                  <div class="col-md-6">
                     <label for="lastname">Email</label>
                     <input type="text" value="<?= $info->email; ?>" class="form-control" name="email" >
                    
                  </div>
               </div>


               

               <div class="col-md-12 form-group">
                  <hr />
               </div>

               <div class="col-md-12 form-group">
                  <div class="col-md-6">
                     <label for="company">User Type</label>
                     <input type="text" value="<?= $info->user_type; ?>" class="form-control" id="autocomplete" name="usertype" value="<?php echo set_value('company'); ?>">
                     <?php echo form_error('company','<p class="text-danger">','</p>'); ?>
                  </div>
               </div>
			  <div class="col-md-12 form-group">
						<div class="address new">
							<button class="btn btn-success btn-block" type="submit">Update</button>
               <a href="<?php echo base_url(); ?>" class="btn btn-default btn-block"><i class="fa fa-chevron-left"></i> back</a>
						</div>
						</div>
                     </form>
<?php endforeach; ?>

</div></div>
</div></form>
</div>
</div>

