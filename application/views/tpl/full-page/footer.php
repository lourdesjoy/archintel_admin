<footer>
   &copy; Copyright <?php echo date('Y', time()); ?> | Developed by <a href="http://www.hexiros.com/">Hexiros</a>
</footer>
</div>
<script type='text/javascript' src='<?php echo base_url('assets/js/jquery.min.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/js/bootstrap.min.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/js/bootbox.min.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/js/jquery.blockUI.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/js/HoldOn.min.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('assets/js/toastr.min.js'); ?>'></script>
<?php

   if(isset($this->_ci_cached_vars))
      foreach($this->_ci_cached_vars as $key => $value){
         $data[$key] = $value;
      }

   if(isset($js))
      foreach($js as $j){
         echo '<script type="text/javascript" src="'.$j.'"></script>';
      }

   if(isset($scripts))
      foreach($scripts as $script){
         $this->load->view($script, $data);
      }
?>
<script type="text/javascript">
   $(".ajax-call").on("click", function(e){
      HoldOn.open({
        theme:"sk-cube-grid",
        message: "please wait..."
      });

      e.preventDefault();

      $.ajax({
         url: e.currentTarget.href,
         type: 'GET',
         success: function(response){
            var r = JSON.parse(response);
            HoldOn.close();
            bootbox.alert(r.message);
         }
      })
   })

   <?php
      if($this->session->ToastSuccessMessage() != null)
         foreach($this->session->ToastSuccessMessage() as $msg){
            echo 'toastr.success("'.$msg.'", "Success!")';
         }

      if($this->session->ToastErrorMessage() != null)
         foreach($this->session->ToastErrorMessage() as $msg){
            echo 'toastr.error("'.$msg.'", "Error!")';
         }

      if($this->session->ToastWarningMessage() != null)
         foreach($this->session->ToastWarningMessage() as $msg){
            echo 'toastr.warning("'.$msg.'", "Warning!")';
         }
   ?>
</script>
</body>
</html>
